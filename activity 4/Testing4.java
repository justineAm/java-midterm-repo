/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JustineAm_Midterm_4;

/**
 *
 * @author 2ndyrGroupA
 */
public class Testing4 {
    
    public static void main(String[] args){
        Customer customer = new Customer(19104890,"Justine Ambrad",20);
        Invoice invoice = new Invoice(1234,customer,1000);
        System.out.println(customer.toString());
        System.out.println(invoice.toString());
        System.out.println(" InVoice after the discount : "+invoice.getAmountAfterDiscount());
        
        System.out.println("\n");
        
        Customer customer1 = new Customer(19104848,"James Lloyd Belda",15);
        Invoice invoice1 = new Invoice(1200,customer1,1200);
        System.out.println(customer1.toString());
        System.out.println(invoice1.toString());
        System.out.println(" InVoice after the discount : "+invoice1.getAmountAfterDiscount());
    }
}
