/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package justineam_midterm_1;

/**
 *
 * @author 2ndyrGroupA
 */
public class Shape {

    private boolean filled;
    private String color;

    public Shape() {
        filled = true;
        color = "Sky Blue";
    }

    public Shape(String colorIn, boolean filledIn) {
        
        filled = filledIn;
        color = colorIn;
        if (filledIn==true) {
            System.out.println("Shape of color is " + getColor() + " and shape is filled");
        } else {
            System.out.println("Shape of color is " + getColor() + " and shape is not filled");
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String colorSetIn) {
        color = colorSetIn;
    }

    public boolean isFilled() {
        if (filled == true) {
            return true;
        } else {
            return false;
        }
    }

    public void setFilled(boolean filledSetIn) {
        filled = filledSetIn;
    }

    public String toString() {
        String isNot = "";
        if (isFilled() == false) {
            isNot = "not ";
        }
        return "\nA Shape with color of " + color + " and is " + isNot + " filled. ";
    }
}
