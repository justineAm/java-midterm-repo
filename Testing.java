/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package justineam_midterm_1;

/**
 *
 * @author 2ndyrGroupA
 */
public class Testing {

    public static void main(String[] args) {
        Shape shape1 = new Shape("Maroon", false);
        
        System.out.println("\n");
        
        Rectangle rec = new Rectangle(12, 6);
        System.out.println(rec.toString());
        System.out.println("Area of Rectangle is = "+rec.getArea());
         System.out.println("Perimeter of Rectangle is = "+rec.getPerimeter());
         
        System.out.println("\n");

        Square square2 = new Square(3.0, "Voilet", true);
        System.out.println(square2.toString());
        System.out.println("Area of square2=" + square2.getArea());
        System.out.println("Perimeter of square2=" + square2.getPerimeter());
        
        System.out.println("\n");
        
        Circle  c1 = new Circle(12,"Brown", true);
        System.out.println(c1.toString());
        System.out.println("Area of Circle is = "+c1.getArea());
         System.out.println("Perimeter of Circle is = "+c1.getPerimeter());

    }
}
