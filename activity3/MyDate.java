/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JustineAm_Midterm_3;

/**
 *
 * @author 2ndyrGroupA
 */
public class MyDate {
    
     public static final int minumumYear  = 1;
    public static final int maximumYear  = 9999;

    public static final int minimumMonth = 1;  
    public static final int maximumMonth = 12;

    public int year;
    public int month;
    public int day;
    public String[] stringMonth = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
         "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public String[] stringDays = {"Sunday", "Monday", "Tuesday", "Wednesday",
         "Thursday", "Friday", "Saturday"};
    public static final int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static final int[] nonLeapYearMonthNumbers = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
    public static final int[] leapYearMonthNumbers = {6, 2, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
    
    

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
    
    public static boolean isValidDate(int year, int month, int day) {
        
        return (minumumYear  <= year   && year  <=  maximumYear)
            && (minimumMonth <= month  && month <= maximumMonth)
            && (1 <= day && day   <= getMonthLastDay(year, month));
    }
    public static int getDayOfWeek(int year, int month, int day)
    {
        if (! isValidDate(year, month, day)) {
            return -1;
        }

        // 1. Based on the first two digit of the year, get the number from the "century" table.
        int centuryNum = 6 - 2*((year / 100) % 4);
        
        // 2. Add to the last two digit of the year.
        int lastTwoDigitsOfYear = year % 100;
        
        // 3. Add to "the last two digit of the year divide by 4, truncate the fractional part".
        int yearNum = lastTwoDigitsOfYear / 4;
        
        // 4. Add to the number obtained from the month table.
        int monthNum = isLeapYear(year) ? leapYearMonthNumbers[month-1] : nonLeapYearMonthNumbers[month-1];
        
        // 5. Add to the day.
        int dayNum = day;
        
        // 6. The sum modulus 7 gives the day of the week, where 0 for SUN, 1 for MON, ..., 6 for SAT.
        return (  centuryNum + lastTwoDigitsOfYear 
                + yearNum    + monthNum + dayNum) % 7; 
    }
    public MyDate(int year, int month, int day) {
        setDate(year, month, day);
    }
     public void setDate(int year, int month, int day)
    {
        if (! isValidDate(year, month, day)) {
            throw new IllegalArgumentException("Invalid year, month, or day!");
        }
        this.year  = year;
        this.month = month;
        this.day   = day;
    }
     
    public static int getMonthLastDay(int year, int month) {
        return daysInMonths[month-1] + (isLeapYear(year) && month == 2 ? 1 : 0 );
    }
    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }
    public void setYear(int year)
    {
        if (year < minumumYear || year > maximumYear) {
            throw new IllegalArgumentException("Invalid year!");
        }
        this.year = year;
    }

    public void setMonth(int month)
    {
        if (month < minimumMonth || month >maximumMonth) {
            throw new IllegalArgumentException("Invalid month!");
        }
        this.month = month;
    }

    public void setDay(int day) 
    {
       
        if (1 > day || day > getMonthLastDay(this.year, this.month)) {
            throw new IllegalArgumentException("Invalid day!");
        }
        this.day = day;
    }
    public String toString() {
        int weekDay = getDayOfWeek(year, month, day);
        return String.format("%1$s %2$d %3$s %4$d", stringDays[weekDay], day, stringMonth[month-1], year);
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof MyDate)) return false;
    
        MyDate otherMyDate = (MyDate) other;
        return (this.year  == otherMyDate.getYear())
            && (this.month == otherMyDate.getMonth())
            && (this.day   == otherMyDate.getDay());
    }
    public MyDate nextDay()
    {
        int maxDay = getMonthLastDay(year, month);
        if (maxDay == day && maximumMonth == month && maximumYear == year) {
            return this;
        }

        if (maxDay == day && maximumMonth == month) {
            setDate(year+1, minimumMonth, 1);
            return this;
        }

        if (maxDay == day) {
            setDate(year, month+1, 1);
            return this;
        }
        
        setDate(year, month, day+1);
        return this;
    }

    public MyDate nextMonth()
    {
        if (maximumMonth== month &&  maximumYear == year) {
            return this;
        }

        int maxDay = (maximumMonth == month)
                ?   getMonthLastDay(year+1, minimumMonth)
                :   getMonthLastDay(year, month+1);

        if (day == getMonthLastDay(year, month)) {
            maxDay = maxDay;
        }
        else if (day > maxDay) {
            maxDay = maxDay;
        }
        else if (day < maxDay) {
            maxDay = day;
        }

        if (maximumMonth == month) {
            setDate(year+1, minimumMonth, maxDay);
            return this;
        }

        setDate(year, month+1, maxDay);
        return this;
    }

    public MyDate nextYear()
    {
        if ( maximumYear == year) {
            return this;
        }

        int maxDay = getMonthLastDay(year+1, month);

        if (day == getMonthLastDay(year, month)) {
            maxDay = maxDay;
        }
        else if (day > maxDay) {
            maxDay = maxDay;
        }
        else if (day < maxDay) {
            maxDay = day;
        }

        setDate(year+1, month, maxDay);
        return this;
    }
    public MyDate previousDay()
    {
        if (1 == day &&minimumMonth == month && minumumYear == year) {
            return this;
        }

        if (1 == day && minimumMonth == month) {
            setDate(year-1,maximumMonth, getMonthLastDay(year-1, maximumMonth));
            return this;
        }

        if (1 == day) {
            setDate(year, month-1, getMonthLastDay(year, month-1));
            return this;
        }
        
        setDate(year, month, --day);
        return this;
    }

    public MyDate previousMonth()
    {
        if (minimumMonth== month && minumumYear == year) {
            return this;
        }

        int maxDay = (minimumMonth== month)
                ?   getMonthLastDay(year-1, maximumMonth)
                :   getMonthLastDay(year, month-1);

        if (day == getMonthLastDay(year, month)) {
            maxDay = maxDay;
        }
        else if (day > maxDay) {
            maxDay = maxDay;
        }
        else if (day < maxDay) {
            maxDay = day;
        }

        if (minimumMonth == month) {
            setDate(year-1, maximumMonth, maxDay);
            return this;
        }

        setDate(year, month-1, maxDay);
        return this;
    }

    public MyDate previousYear()
    {
        if (minumumYear == year) {
            return this;
        }

        int maxDay = getMonthLastDay(year-1, month);

        if (day == getMonthLastDay(year, month)) {
            maxDay = maxDay;
        }
        else if (day > maxDay) {
            maxDay = maxDay;
        }
        else if (day < maxDay) {
            maxDay = day;
        }

        setDate(year-1, month, maxDay);
        return this;
    }
    
}
