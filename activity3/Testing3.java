/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JustineAm_Midterm_3;

/**
 *
 * @author 2ndyrGroupA
 */
public class Testing3 {

    public static void main(String[] args) {
        MyDate currentDate = new MyDate(2021, 3, 27);
        System.out.println("Current date: " + currentDate);
        System.out.println("\n");
        
        MyDate date1 = new MyDate(2000, 11, 05);
        System.out.println("Choosen date is " + date1);
        System.out.println("The next day is " + date1.nextDay());
        System.out.println("The next next day is " + date1.nextDay());
        System.out.println("The next month is " + date1.nextMonth());
        System.out.println("The next year is " + date1.nextYear());
        
        System.out.println("\n");
        
        MyDate date2 = new MyDate(2005, 04, 29);
        System.out.println(date2.getDay());
        System.out.println("Choosen date is " + date2);
        System.out.println("The other day is " + date2.previousDay());
        System.out.println("The other day is " + date2.previousDay());
        System.out.println("The previous month is " + date2.previousMonth());
        System.out.println("The previous year is " + date2.previousYear());
        
        
      
    }
}
