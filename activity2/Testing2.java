/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JustineAm_midterm_2;

import java.util.Date;

/**
 *
 * @author 2ndyrGroupA
 */
public class Testing2 {

    public static void main(String[] args) {
        Customer c1 = new Customer("James Lloyd Belda", true, "Gold");
        Customer c2 = new Customer("Romeo Rodemio", false, "Premium");

        Visit v1 = new Visit(c1, new Date());
        System.out.println(c1.toString());
        System.out.println(v1.toString());
        v1.setProductExpense(400);
        v1.setServiceExpense(80);
        System.out.println("Product Expense is "+v1.getProductExpense());
        System.out.println("Service Expense is "+v1.getServiceExpense());
        System.out.println("Total expense made by " + v1.getCustomerName() + " = " + v1.getTotalExpense());
        System.out.println("\n");
        
        
        Visit v2 = new Visit(c2, new Date());
        System.out.println(c2.toString());
        System.out.println(v2.toString());
        v2.setProductExpense(100);
        v2.setServiceExpense(120);
        System.out.println("Product Expense is "+v2.getProductExpense());
        System.out.println("Service Expense is "+v2.getServiceExpense());
        System.out.println("Total expense made by " + v2.getCustomerName() + " = " + v2.getTotalExpense());
    }
}
